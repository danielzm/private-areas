module.exports = function PrivateAreasModule(pb) {

    /**
     * PrivateAreas - A Private Areas plugin for PencilBlue
     * Create private pages and manage permissions by user/area
     *
     * @author Daniel Zamorano @danielzm <daniel.zamorano.m@gmail.com>
     * @copyright 2015 Daniel Zamorano
     */

    function PrivateAreas() {}

    PrivateAreas.onInstall = function (cb) {
        var self = this;

        self.createPrivateAreaCustomObject(function (err, objectType) {
            if (err) {
                customObjectError(err);
                cb(err, true);
            } else {
                cb(null, true);
            }
        });
        return;
    };

    PrivateAreas.createPrivateAreaCustomObject = function (cb) {
        var objectName = 'private_area',
            objectModel = {
                name: objectName,
                fields: {
                    enabled: {
                        field_type: 'boolean'
                    },
                    allowed_users: {
                        field_type: 'child_objects',
                        object_type: 'user'
                    }
                }
            };

        this.createCustomObject(objectName, objectModel, cb);

        return;
    };

    PrivateAreas.createCustomObject = function (objectName, objectModel, cb) {
        var customObjectService = new pb.CustomObjectService();

        customObjectService.loadTypeByName(objectName, function (err, objectType) {
            if (!objectType) {
                pb.log.info('Creating ' + objectName + ' custom object');
                customObjectService.saveType(objectModel, function (err, objectType) {
                    if (!err) {
                        pb.log.info(objectName + ' created.');
                        cb(null, objectType);
                    } else {
                        cb(err);
                    }
                });
            } else {
                pb.log.debug(objectName + ' existent, nothing to do.');
                cb(null, objectType);
            }
        });
    };

    PrivateAreas.onUninstall = function (cb) {
        cb(null, true);
    };

    PrivateAreas.onStartup = function (cb) {
        pb.AdminSubnavService.registerFor('plugin_settings', function (navKey, localization, data) {
            if (data.plugin.uid === 'private_areas') {
                return [{
                    name: 'home_page_settings',
                    title: 'Home page settings',
                    icon: 'home',
                    href: '/admin/plugins/private_areas/settings/home_page'
                }];
            }
            return [];
        });
        cb(null, true);
    };
    PrivateAreas.onShutdown = function (cb) {
        cb(null, true);
    };

    return PrivateAreas;
};
